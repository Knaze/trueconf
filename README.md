**ЗАДАНИЕ:**

Создать маленькое приложение на Slim Framework предоставляющие REST API по работе с сущностью User.

REST API должно удовлетворять следующие возможности:
• Добавление User
• Получение списка User
• Получение User по Id
• Редактирование User по Id
• Удаление User по Id

REST API должно работать с форматом данных JSON.
Сущность User должно состоять минимум из следующих полей:
• Идентификатор пользователя
• Отображаемое имя

Вы можете использовать дополнительные поля, если считаете нужным.
В качестве хранилища данных нужно использовать файл в формате JSON.

-- 
-- Table structure for 'tasks'
-- 

CREATE TABLE `users`.`users`(

    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `user_name` VARCHAR(255) NOT NULL,
    `user_email` VARCHAR(200) NOT NULL,
    PRIMARY KEY(`id`),
    UNIQUE `user_email`(`user_email`)
) ENGINE = InnoDB;

**ПРОВЕРКА**

#### • Добавление User (метод POST)

`http://localhost:8080/api_v1/user/insert`

#### • Получение списка User (метод GET)
`http://localhost:8080/api_v1/user/list`

#### • Получение User по Id (метод GET)
`http://localhost:8080/api_v1/user/{id}`

#### • Редактирование User по Id (метод POST)
`http://localhost:8080/api_v1/user/update/{id}`

#### • Удаление User по Id (метод POST)
`http://localhost:8080/api_v1/user/delete/{id}`
