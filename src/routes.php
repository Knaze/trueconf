<?php
// Routes
$app->get('/[{name}]', function ($request, $response, $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");
    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});

// select all users
$app->get('/api_v1/user/list', function ($request, $response, $args) {
    $stmt = $this->db->prepare("SELECT * FROM users ORDER by id ASC");
    $stmt->execute();
    $users_list = $stmt->fetchall();
    return $this->response->withJson($users_list);
});

// select user by id
$app->get('/api_v1/user/[{id}]', function ($request, $response, $args) {
    $stmt = $this->db->prepare("SELECT * FROM users WHERE id = :id LIMIT 1");
    $stmt->bindParam("id", $args['id'], PDO::PARAM_INT);
    $stmt->execute();
    $user = $stmt->fetchone();
    return $this->response->withJson($user);
});

// insert new user
$app->post('/api_v1/user/insert', function ($request, $response) {
    $user = $request->getParsedBody();
    $sql = "INSERT IGNORE INTO users (user_name, user_email) VALUES (:name, :email)";
    $stmt = $this->db->prepare($sql);
    $stmt->bindParam("name", $user['name']);
    $stmt->bindParam("email", $user['email']);
    $stmt->execute();
    $user['id'] = $this->db->lastInsertId();
    return $this->response->withJson($user);
});

// update user by id
$app->post('/api_v1/user/update/[{id}]', function ($request, $response, $args) {
    $input = $request->getParsedBody();
    $sql = "UPDATE users SET user_name = :name, user_email = :email WHERE id=:id";
    $stmt = $this->db->prepare($sql);
    $stmt->bindParam("id", $args['id']);
    $stmt->bindParam("name", $input['name']);
    $stmt->bindParam("email", $input['email']);
    $stmt->execute();
    $input['id'] = $args['id'];
    return $this->response->withJson($input);
});

// DELETE a user with given id
$app->delete('/api_v1/user/delete/[{id}]', function ($request, $response, $args) {
    $stmt = $this->db->prepare("DELETE FROM users WHERE id=:id");
    $stmt->bindParam("id", $args['id']);
    $stmt->execute();
    return "success";
});
