<?php
return [
    'settings' => [
        'displayErrorDetails' => true,

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => __DIR__ . '/../logs/app.log',
        ],
        'db' => [
            'host' => 'localhost',
            'dbname' => 'trueconf',
            'user' => 'root',
            'pass' => '',
            'collation' => 'utf8_general_ci',
            'charset' => 'utf8',
            'prefix' => ''
        ],
    ],
];
